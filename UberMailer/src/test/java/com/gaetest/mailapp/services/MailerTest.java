package com.gaetest.mailapp.services;

import static org.junit.Assert.*;

import org.junit.Test;
import org.mockito.Mockito;

import com.gaetest.mailapp.pojo.MailRequest;

public class MailerTest {

	@Test
	public void testIsValidSuccess() {
		Mailer mailer=new Mailer();
		MailRequest mreq = getTestMailRequest();
		assertTrue(mailer.isValid(mreq));
	}

	@Test
	public void testIsValidFailure() {
		Mailer mailer=new Mailer();
		MailRequest mreq=new MailRequest.Builder()
				.to("bar@foo.com")
				.subject("test")
				.message("hey there!")
				.build();
		assertFalse(mailer.isValid(mreq));
	}

	@Test
	public void testSendMailWithPrimaryMailer() {
		MailGunMailer mgMailer = Mockito.mock(MailGunMailer.class);
		MailRequest mreq = getTestMailRequest();
		Mockito.when(mgMailer.sendMail(mreq)).thenReturn(true);
		Mailer mailer = new Mailer(mgMailer, null); // we don't care about the other mailer for this test
		assertTrue(mailer.sendMail(mreq));
	}

	@Test
	public void testSendMailWithBackupMailer() {
		MailRequest mreq = getTestMailRequest();
		MailGunMailer mgMailer = Mockito.mock(MailGunMailer.class);
		Mockito.when(mgMailer.sendMail(mreq)).thenReturn(false);
		SendGridMailer sgMailer = Mockito.mock(SendGridMailer.class);
		Mockito.when(sgMailer.sendMail(mreq)).thenReturn(true);
		
		Mailer mailer = new Mailer(mgMailer, sgMailer); 
		assertTrue(mailer.sendMail(mreq));
	}
	
	@Test
	public void testSendMailBothTheMailersDown() {
		MailRequest mreq = getTestMailRequest();
		MailGunMailer mgMailer = Mockito.mock(MailGunMailer.class);
		Mockito.when(mgMailer.sendMail(mreq)).thenReturn(false);
		SendGridMailer sgMailer = Mockito.mock(SendGridMailer.class);
		Mockito.when(sgMailer.sendMail(mreq)).thenReturn(false);
		
		Mailer mailer = new Mailer(mgMailer, sgMailer); 
		assertFalse(mailer.sendMail(mreq));
	}
	
	private MailRequest getTestMailRequest() {
		return new MailRequest.Builder()
				.from("foo@bar.com")
				.to("bar@foo.com")
				.subject("test")
				.message("hey there!")
				.build();
	}
	
}
