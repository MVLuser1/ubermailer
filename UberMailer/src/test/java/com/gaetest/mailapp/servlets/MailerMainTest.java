package com.gaetest.mailapp.servlets;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.junit.Test;
import org.mockito.Mockito;

import com.gaetest.mailapp.pojo.MailRequest;
import com.gaetest.mailapp.services.Mailer;

public class MailerMainTest {

	@Test
	public void testDoPostSuccess() throws IOException {
		
		MailerMain mainServlet = new MailerMain();
		
		MailRequest mreq = getTestMailRequest();
		Mailer mailer = Mockito.mock(Mailer.class);
		Mockito.when(mailer.isValid(Mockito.any(MailRequest.class))).thenReturn(true);
		Mockito.when(mailer.sendMail(Mockito.any(MailRequest.class))).thenReturn(true);
		mainServlet.setMailer(mailer);
		
		HttpServletRequest request = getMockServletRequest(mreq);
		MockHttpServletResponse response = new MockHttpServletResponse();
		
		mainServlet.doPost(request, response);
	    assertEquals(200, response.getStatus());
	}
	
	@Test
	public void testDoPostInvalidInputs() throws IOException {
		MailerMain mainServlet = new MailerMain();
		
		MailRequest mreq = getTestMailRequest();
		Mailer mailer = Mockito.mock(Mailer.class);
		Mockito.when(mailer.isValid(Mockito.any(MailRequest.class))).thenReturn(false);
		mainServlet.setMailer(mailer);

		HttpServletRequest request = getMockServletRequest(mreq);
		MockHttpServletResponse response = new MockHttpServletResponse();
		
		mainServlet.doPost(request, response);

	    assertEquals(400, response.getStatus());
	}
	
	@Test
	public void testDoPostServerError() throws IOException {
		MailerMain mainServlet = new MailerMain();
		
		MailRequest mreq = getTestMailRequest();
		Mailer mailer = Mockito.mock(Mailer.class);
		Mockito.when(mailer.isValid(Mockito.any(MailRequest.class))).thenReturn(true);
		Mockito.when(mailer.sendMail(Mockito.any(MailRequest.class))).thenReturn(false);
		mainServlet.setMailer(mailer);

		HttpServletRequest request = getMockServletRequest(mreq);
		MockHttpServletResponse response = new MockHttpServletResponse();

		mainServlet.doPost(request, response);
	    assertEquals(500, response.getStatus());
	}
	

	private MailRequest getTestMailRequest() {
		return new MailRequest.Builder()
				.from("foo@bar.com")
				.to("bar@foo.com")
				.subject("test")
				.message("hey there!")
				.build();
	}
	
	private HttpServletRequest getMockServletRequest(MailRequest mreq) {
		HttpServletRequest req = Mockito.mock(HttpServletRequest.class);
		Mockito.when(req.getParameter("from")).thenReturn(mreq.getFrom());
		Mockito.when(req.getParameter("to")).thenReturn(mreq.getTo());
		Mockito.when(req.getParameter("subject")).thenReturn(mreq.getSubject());
		Mockito.when(req.getParameter("message")).thenReturn(mreq.getMessage());
		return req;
	}
}
