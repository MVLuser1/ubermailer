package com.gaetest.mailapp.pojo;

public class MailRequest {
	String from;
	String to;
	String subject;
	String message;
	
	public MailRequest(Builder builder) {
		this.from = builder.from;
		this.to = builder.to;
		this.subject = builder.subject;
		this.message = builder.message;
	}
	
	public String getFrom() {
		return from;
	}

	public String getTo() {
		return to;
	}

	public String getSubject() {
		return subject;
	}

	public String getMessage() {
		return message;
	}

	public static class Builder {
		String from;
		String to;
		String subject;
		String message;
		
		public MailRequest build() {
		      return new MailRequest(this);
	    }
		 
		public Builder from(String from) {
	      this.from = from;
	      return this;
	    }
		public Builder to(String to) {
		      this.to = to;
		      return this;
		}
		public Builder subject(String subject) {
		      this.subject = subject;
		      return this;
		}
		public Builder message(String message) {
		      this.message = message;
		      return this;
	    }
	}
	
	
}
