package com.gaetest.mailapp.services.api;

import com.gaetest.mailapp.pojo.MailRequest;

public interface MailerApi {

	//public boolean isServiceUp();
	public boolean sendMail(MailRequest mreq);
}
