package com.gaetest.mailapp.services;

import java.io.IOException;

import com.gaetest.mailapp.pojo.MailRequest;
import com.gaetest.mailapp.services.api.MailerApi;
import com.sendgrid.Content;
import com.sendgrid.Email;
import com.sendgrid.Mail;
import com.sendgrid.Method;
import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;

public class SendGridMailer implements MailerApi {
	private static final String API_KEY="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";
	private SendGrid sg = new SendGrid(API_KEY);

	private Mail toMail(MailRequest mreq) {
		Email from = new Email(mreq.getFrom());
	    Email to = new Email(mreq.getTo());
	    String subject = mreq.getSubject();
	    Content content = new Content("text/plain", mreq.getMessage());
	    Mail mail = new Mail(from, subject, to, content);
	    return mail;
	}
	
	@Override
	public boolean sendMail(MailRequest mreq) {
		Mail mail = toMail(mreq);
	    Request request = new Request();
        request.setMethod(Method.POST);
        request.setEndpoint("mail/send");
        try {
		  request.setBody(mail.build());
	      Response response = sg.api(request);
	      return (response.getStatusCode()>=200 && response.getStatusCode()<300)? true : false;
        } catch (IOException e) {
          e.printStackTrace();
        }
		return false;
	}
}
