package com.gaetest.mailapp.services;

import java.util.List;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;

import com.gaetest.mailapp.pojo.MailRequest;
import com.gaetest.mailapp.services.api.MailerApi;

public class MailGunMailer implements MailerApi {
	
	private static final String API_KEY="aa1cb005aa66580a452900f87cf7d206-a3d67641-dd454dfc";
	private static final String URL = "https://api.mailgun.net/v3/sandbox1544fa14572748a48e611ddb841bf181.mailgun.org/messages";

	@Override
	public boolean sendMail(MailRequest mreq) {

		HttpClient client = HttpClientBuilder.create().build();
		HttpPost post = new HttpPost(URL);

		List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
		urlParameters.add(new BasicNameValuePair("from", mreq.getFrom()));
		urlParameters.add(new BasicNameValuePair("to", mreq.getTo()));
		urlParameters.add(new BasicNameValuePair("subject", mreq.getSubject()));
		urlParameters.add(new BasicNameValuePair("text", mreq.getMessage()));
		
		try {
			post.setEntity(new UrlEncodedFormEntity(urlParameters));
			HttpResponse response = client.execute(post);
			return (response.getStatusLine().getStatusCode()==200)? true : false;
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return false;
	}

}
