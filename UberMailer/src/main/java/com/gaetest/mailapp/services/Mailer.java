package com.gaetest.mailapp.services;

import org.apache.commons.lang3.StringUtils;

import com.gaetest.mailapp.pojo.MailRequest;

public class Mailer {
	
	MailGunMailer mgMailer;
	SendGridMailer sgMailer;

	public Mailer() {
		mgMailer = new MailGunMailer();
		sgMailer = new SendGridMailer();
	}
	
	public Mailer(MailGunMailer mgMailer, SendGridMailer sgMailer) {
		this.mgMailer=mgMailer;
		this.sgMailer=sgMailer;
	}
	
	public boolean isValid(MailRequest mreq) {
		if (mreq==null || StringUtils.isBlank(mreq.getFrom()) || StringUtils.isBlank(mreq.getTo()))
				return false;
		return true;
	}

	public boolean sendMail(MailRequest mreq) {
		boolean isSuccess = mgMailer.sendMail(mreq); 
		if (!isSuccess) {
			isSuccess = sgMailer.sendMail(mreq);
		}
		return isSuccess;
	}

}
