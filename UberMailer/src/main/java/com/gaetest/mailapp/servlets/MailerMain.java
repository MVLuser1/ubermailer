package com.gaetest.mailapp.servlets;

import java.io.IOException;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.gaetest.mailapp.pojo.MailRequest;
import com.gaetest.mailapp.services.Mailer;


@SuppressWarnings("serial")
@WebServlet(
    name = "MailerMain",
    urlPatterns = {"/api/m"}
)
public class MailerMain extends HttpServlet {
  
  Mailer mailer = new Mailer();

  @Override
  public void doPost(HttpServletRequest request, HttpServletResponse response) 
      throws IOException {
	
	  MailRequest mailReq = new MailRequest.Builder()
			  .from(request.getParameter("from"))
			  .to(request.getParameter("to"))
			  .subject(request.getParameter("subject"))
			  .message(request.getParameter("message"))
			  .build();

	  if (mailer.isValid(mailReq)) {
		  	if (mailer.sendMail(mailReq)) {
		  		response.setStatus(200);
			    response.setContentType("text/plain");
			    response.setCharacterEncoding("UTF-8");
			    response.getWriter().print("Message sent!\r\n");
		  	} else {
		  		response.sendError(500, "Server Error");
		  	}
	  } else {
		  response.sendError(400, "Invalid inputs");
	  }
	  
  }

	public void setMailer(Mailer mailer) {
		this.mailer = mailer;
	}
  
}